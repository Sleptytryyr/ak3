@echo off

attrib +h "D:\PROEKTU\lab\pz-23y-1\Kovalova\batch\Pruhovana"

xcopy /? > "D:\PROEKTU\lab\pz-23y-1\Kovalova\batch\NePruhovana\copyhelp.txt"

xcopy "D:\PROEKTU\lab\pz-23y-1\Kovalova\batch\NePruhovana\copyhelp.txt" "D:\PROEKTU\lab\pz-23y-1\Kovalova\batch\Pruhovana\copied_copyhelp.txt"

echo.
echo Режим підказки
echo.

:show_help
echo Переміщення файлів в інший каталог.
echo.
echo Синтаксис:
echo   move_files.bat [початковий_каталог] [цільний_каталог]
echo.
echo Приклад використання:
echo   move_files.bat "путь\до\початкового\каталогу" "путь\до\цільного\каталогу"
exit /b

if "%~1"=="" (
    call :show_help
)

set "source_dir=%~1"
set "destination_dir=%~2"

move /Y /A "%source_dir%\*" "%destination_dir%"

if %errorlevel% neq 0 (
    echo Помилка: Файли не були переміщені.
    exit /b %errorlevel%
) else (
    echo Успіх: Файли успішно переміщені.
    exit /b 0
)

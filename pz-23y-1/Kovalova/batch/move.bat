@echo off
set "source_dir=D:\PROEKTU\lab\pz-23y-1\Kovalova\batch\NePruhovana"
set "destination_dir=D:\PROEKTU\lab\pz-23y-1\Kovalova\move"

move "%source_dir%\*" "%destination_dir%"




:show_help
echo Переміщення файлів в інший каталог.
echo.
echo suntaksus:
echo   move.bat [start] [end]
echo.
echo example:
echo   move.bat "D:\PROEKTU\lab\pz-23y-1\Kovalova\batch\NePruhovana" "D:\PROEKTU\lab\pz-23y-1\Kovalova\move"
exit /b

if "%~1"=="" (
    call :show_help
)


set "source_dir=%~1"
set "destination_dir=%~2"


move "%source_dir%\*" "%destination_dir%"

if %errorlevel% neq 0 (
    echo  Error.
    exit /b %errorlevel%
) else (
    echo Good.
    exit /b 0
)
